//create
db.users.insertMany([
        {
            firstName: "Harry",
            lastName: "Styles",
            email: "harrystyles@mail.com",
            password: "hstyles123",
            isAdmin: false
        },
        {
            firstName: "Louis",
            lastName: "Tomlinson",
            email: "louistomlinson@mail.com",
            password: "ltomlinson123",
            isAdmin: false
        },
        {
            firstName: "Liam",
            lastName: "Payne",
            email: "liampayne@mail.com",
            password: "lpayne123",
            isAdmin: false
        },
        {
            firstName: "Niall",
            lastName: "Horran",
            email: "niallhorran@mail.com",
            password: "nhorran123",
            isAdmin: false
        },
        {
            firstName: "Zayn",
            lastName: "Malik",
            email: "zaynmalik@mail.com",
            password: "zmalik123",
            isAdmin: false
        }
])
        

db.newCourses.insertMany([
        {
            name: "English 101",
            price: 1000,
            isActive: false
        },
        {
            name: "English 105",
            price: 2000,
            isActive: false
        },
        {
            name: "English 109",
            price: 3000,
            isActive: false
        }    
])

// read
db.users.find()

// update
db.users.updateOne({}, {$set: {isAdmin: true}})
db.newCourses.updateOne({name:"English 109"}, {$set: {isActive:true}})

//delete
db.newCourses.deleteMany({isActive: false})